function mobileMenuInit(state) {
 $('.mobile-menu').toggleClass('active');
 $('body').toggleClass('menu-active');
 $('.mobile-menu-trigger--sub').removeClass('active');
}

function mobileMenuChildInit(state) {
    state.addClass('active');
}

function mobileMenuBack(state) {
    var closestActive = state.closest('ul').siblings();
    console.log(closestActive);
    closestActive.removeClass('active');
}

$('#mobile-menu-trigger').click(function(e){
    e.preventDefault();
    mobileMenuInit();
})

$('.mobile-menu-trigger--sub').click(function(e){
    var clickedElement = $(this);
    mobileMenuChildInit(clickedElement);
})

$('.mobile-menu--back').click(function(e){ 
    var clickedElement = $(this);
    mobileMenuBack(clickedElement);
})

$('#header-search').on('shown.bs.collapse', function (e) {
    $('#search').focus();
    e.preventDefault(); // the important thing I think
    e.stopPropagation();
    console.log("hello");
    setTimeout(function(e) { 
        $( "#search" ).focus();
        console.log("goodbye");
        }
    ,100) 
})




export default mobileMenuInit;
export default mobileMenuChildInit;
export default mobileMenuBack;
