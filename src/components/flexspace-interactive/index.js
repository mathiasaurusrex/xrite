function defaultInteractiveAccordion(state) {
    var parentSpace = state.closest('.flex-space-interactive');
    parentSpace.toggleClass("active");
}

$('.flex-space-button').click(function(e){ 
    var clickedElement = $(this);
    defaultInteractiveAccordion(clickedElement);
})


$('*[class^="radio-open"]').on('change', function () {
    if (!this.checked) return
    $('.collapse.' + $(this).attr('class')).slideDown();
});

$('*[class^="radio-close"]').on('change', function () {
    if (!this.checked) return
    $('.collapse.' + $(this).attr('class')).slideUp();
});


export default defaultInteractiveAccordion;