$('.banner-carousel').slick({
    dots:true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 8000,
});

$('.product-banner-carousel').slick({
    dots:true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 8000,
});


$('.hero-slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.hero-sub-slider'
});

$('.hero-sub-slider').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  asNavFor: '.hero-slider',
  dots: true,
  centerMode: false,
  focusOnSelect: true
});

$('.media-object--slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  asNavFor: '.testmonials-slider',
  dots: true,
  centerMode: false,
  focusOnSelect: true,
  mobileFirst: true,

  responsive: [
    
    {
      breakpoint: 1000,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1
      }
    }
  ]
});


$('.coloratti--slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: true,
  fade: true,
  asNavFor: '.coloratti-sub--slider'
});

$('.coloratti-sub--slider').slick({
  slidesToShow: 10,
  slidesToScroll: 1,
  asNavFor: '.coloratti--slider' ,
  dots: false,
  focusOnSelect: true
  // responsive: [
  //   {
  //     breakpoint: 1024,
  //     settings: "unslick"
  //   }
  // ]
})


$('.coloratti--lightbox--slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  asNavFor: '.coloratti--slider',
  dots: false,
  focusOnSelect: true,
  arrows: true
})



$(function () {
  $('[data-toggle="popover"]').popover()
})

// Old Video Modal Stuff
var videoPlayer;

$('.video-modal').on('hidden.bs.modal', function(){
        
        videoPlayer.destroy();

});

$('.video-modal').on('shown.bs.modal', function(event){

        var button = $(event.relatedTarget);
        var playerID = button.data('playerid')
        console.log(playerID);
        videoPlayer =  OO.Player.create('ooyalaplayer', playerID, {
          height: 360,
          width: 640,
          autoplay: 1
        });
});

$('#coloratti-lightbox').on('shown.bs.modal', function (e) {
    $('#coloratti-lightbox').delay( 1000 ).resize();
    console.log("help")
    $('.coloratti--lightbox--slider').reInit();
});



$('#coloratti-lightbox').on('hide.bs.modal', function (e) {
  console.log("close")
});


$(document).ready(function() {
  $(".product-listing .flex-space-copy").dotdotdot();
});


$('.form-group input').blur(function(){
    var tmpval = $(this).val();
    if(tmpval == '') {
        $(this).addClass('empty');
        $(this).removeClass('not-empty');
    } else {
        $(this).addClass('not-empty');
        $(this).removeClass('empty');
    }
});

$('.form-group select').blur(function(){
    var tmpval = $(this).val();
    if(tmpval == '') {
        $(this).addClass('empty');
        $(this).removeClass('not-empty');
    } else {
        $(this).addClass('not-empty');
        $(this).removeClass('empty');
    }
});

if (navigator.userAgent.indexOf('Safari') != -1 && 
    navigator.userAgent.indexOf('Chrome') == -1) {
        document.body.className += " safari";
    }

$(".primary-nav li").mouseenter(function() {
  $('#search').blur();
});
// function validateRadio(){
//   var hiddenField = $(".hidden-radio");
//   $(hiddenField).each(function(){
//     if ($(this).is(':checked') === true ) {
//       $(this).closest(".row").addClass('error');
//       $(this).closest("error").find(".field-validation-valid").addClass(".field-validation-invalid").removeClass(".field-validation-valid");
//     }
//     else if ($(this).is(':checked') === false ) {
//        $(this).closest(".row").removeClass('error');
//       $(this).closest("error").find(".field-validation-invalid").addClass(".field-validation-valid").removeClass(".field-validation-invalid");
//     }
//   });
//   $(".error .checkbox input").change(function(){
//     $(this).closest(".row").removeClass('error');
//     $(this).closest("error").find(".field-validation-invalid").addClass(".field-validation-valid").removeClass(".field-validation-invalid");
//   });
// }





// if ($(".hidden-radio").is(':checked') == true ) {
//   $(this).closest(".row").addClass('error');
// }