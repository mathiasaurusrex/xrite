(function () {
    window.onload = function() {
      theTruffleShuffle();
    };

// Initialize Dragula individually on each row. 
// Initializing them on each row prevents them from being dragged between one another

dragula([document.getElementById('row-1')]);
dragula([document.getElementById('row-2')]);
dragula([document.getElementById('row-3')]);
dragula([document.getElementById('row-4')]);

// Sets a new analytics object
var aObject = {date:"",score:"n/a",gender:"n/a",age:"n/a"};

// Push the current date time into the analytics object
aObject.date = new Date();

// Calculating the Hues:
// 1. Push hues into the array
// 2. Compare the data position ( the answer ) to the actual position
// 3. Push the sum of the data position and actual position into the array.
function calculateHues(){
    // Grab all the hues and add them to Hue Array ( hArray)
    hArray = $('.hue-box').show();

    // Grabs the amount of hue's in the array.
    var hArrayLength = hArray.length;

    // Variable to save the position of each item in the Array
    hArrayPosition = [];

    // Variable to save the position of each item in the Array
    hArrayScore = [];

    // Variable to save the total sum of the array.
    totalScore = 0;

    dataSum = [];

    // Iterate through the array
    for ( i = 0; i < hArrayLength; i++) {

        // pushes the data attribute into a variable
        var theData = $(hArray[i]).data();

        // compares the data attribute to the current position of the hue.
        dataSum = theData.position - i;

        // strips out all the negative numbers and makes them positive
        dataSum = Math.abs(dataSum);

        // Pushes that comparison into the hArrayPosition array
        // Use hArrayPosition for the plotting of the radar grid.
        // theData.position = the origin ( correct ) location
        // dataSum = how far offset the hue is from it's original position
        hArrayPosition.push([theData.position, dataSum]);

        // score array.
        hArrayScore.push(dataSum);

        // Reduce all the individual numbers in the hArrayScore into the sum.
        totalScore = hArrayScore.reduce(function(a, b) {
          return a + b;
        });
    }

}

// Does some stuff on clicking a button.
$('#calculate').click(function(){

    $('.hue-test').collapse();
    $('.hue-test-results').collapse();
    
    // fires some functions
    calculateHues();
    createGraph();

    // Push score to object
    aObject.score = totalScore;

    //Update hue-score container to show the total score.
    $('.hue-score').text(totalScore);

});

$("#compare").click(function(){
    // Collapses and Shows the appropriate containers
    $(".compare-container").collapse();
    $(".compare-form-container").collapse();

    // Sets the variable for age, gender
    var compareAgeRange = $('.toggle-age-range option:selected').text();
    var compareGender = $('input[name=sex]:checked').val();

    // Sets the containers
    $('#gender').text(compareGender);
    $('#age').text(compareAgeRange);
    
    //Push to analytics object
    aObject.gender = compareGender;
    aObject.age = compareAgeRange;

    // Best is always going to be a perfect score
    $('#best').text('0');

    // Worst is going to be static for now until we have some data.
    // Unclear if this will change.
    $('#worst').text('89');

});

// Guess what this function does?
function createGraph() {
    //Creates a graph!

    var radarChartData = {
        // Labels for the Radar Chart
        labels : ["1", "2", "3","4", "5", "6", "7", "8", "9", "10",
                  "11", "12", "13","14", "15", "16", "17", "18", "19", "20",
                  "21", "22", "23","24", "25", "26", "27", "28", "29",
                  "30", "31", "32", "33","34", "35", "36", "37", "38", "39",
                  "40"],
        // Set the Data Set
        datasets : [
            {
                fillColor : "rgba(220,220,220,0.5)",
                strokeColor : "rgba(220,220,220,0.8)",
                highlightFill: "rgba(220,220,220,0.75)",
                highlightStroke: "rgba(220,220,220,1)",
                // data is the object property that is plotting the dots.
                // hArrayScore is an array full of offset values
                data : hArrayScore
            }
        ]

    }
    // Renders a new chart on the canvas element and gives it the Radar properties.
    window.myRadar = new Chart(document.getElementById("canvas").getContext("2d")).Radar(radarChartData, {
        // Makes it responsive.
        responsive: true
    });
    
}

// Color Blind Toggle -- 
// Looks for the dropdown and adds a class based on the selection

$('.toggle-colorblindness').change(function(){
    var str = "";
    $( ".toggle-colorblindness option:selected" ).each(function() {
        str += $( this ).text() + " ";
        $(".hue-test > div").removeClass( );
        $(".hue-test > div").addClass( str );
    });
    theTruffleShuffle();
});

// Shuffles the hues.
function theTruffleShuffle(){
    theRows = ['#row-1', '#row-2', '#row-3', '#row-4']
    for (i = 0; i < theRows.length; i++){
        var parent = $(theRows[i]);
        var divs = parent.children();
        while (divs.length) {
            parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
        };
    }
}
})();

