
/* 
 /src/scripts/bundle.js 
 */ 
/******/'use strict';

(function (modules) {
	// webpackBootstrap
	/******/ // The module cache
	/******/var installedModules = {};

	/******/ // The require function
	/******/function __webpack_require__(moduleId) {

		/******/ // Check if module is in cache
		/******/if (installedModules[moduleId])
			/******/return installedModules[moduleId].exports;

		/******/ // Create a new module (and put it into the cache)
		/******/var module = installedModules[moduleId] = {
			/******/exports: {},
			/******/id: moduleId,
			/******/loaded: false
			/******/ };

		/******/ // Execute the module function
		/******/modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

		/******/ // Flag the module as loaded
		/******/module.loaded = true;

		/******/ // Return the exports of the module
		/******/return module.exports;
		/******/
	}

	/******/ // expose the modules object (__webpack_modules__)
	/******/__webpack_require__.m = modules;

	/******/ // expose the module cache
	/******/__webpack_require__.c = installedModules;

	/******/ // __webpack_public_path__
	/******/__webpack_require__.p = "";

	/******/ // Load entry module and return exports
	/******/return __webpack_require__(0);
	/******/
})(
/************************************************************************/
/******/[
/* 0 */
function (module, exports, __webpack_require__) {

	'use strict';

	function _interopRequireDefault(obj) {
		return obj && obj.__esModule ? obj : { 'default': obj };
	}

	var _appIndexJs = __webpack_require__(1);

	var _appIndexJs2 = _interopRequireDefault(_appIndexJs);

	var _componentsAccordion = __webpack_require__(5);

	var _componentsAccordion2 = _interopRequireDefault(_componentsAccordion);

	var _componentsModal = __webpack_require__(6);

	var _componentsModal2 = _interopRequireDefault(_componentsModal);

	var _componentsCarousel = __webpack_require__(7);

	var _componentsCarousel2 = _interopRequireDefault(_componentsCarousel);

	var _componentsBanner = __webpack_require__(8);

	var _componentsBanner2 = _interopRequireDefault(_componentsBanner);

	var _componentsForm = __webpack_require__(9);

	var _componentsForm2 = _interopRequireDefault(_componentsForm);

	var _componentsNavigation = __webpack_require__(10);

	var _componentsNavigation2 = _interopRequireDefault(_componentsNavigation);

	var _componentsFlexspaceInteractive = __webpack_require__(11);

	var _componentsFlexspaceInteractive2 = _interopRequireDefault(_componentsFlexspaceInteractive);

	var app = new _appIndexJs2['default']();

	// register modules
	app.registerModule(_componentsAccordion2['default']);
	app.registerModule(_componentsModal2['default']);
	app.registerModule(_componentsCarousel2['default']);
	app.registerModule(_componentsForm2['default']);

	/***/
},
/* 1 */
function (module, exports, __webpack_require__) {

	// import R from 'ramda';
	'use strict';

	Object.defineProperty(exports, '__esModule', {
		value: true
	});

	var _createClass = (function () {
		function defineProperties(target, props) {
			for (var i = 0; i < props.length; i++) {
				var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ('value' in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
			}
		}return function (Constructor, protoProps, staticProps) {
			if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
		};
	})();

	function _interopRequireDefault(obj) {
		return obj && obj.__esModule ? obj : { 'default': obj };
	}

	function _classCallCheck(instance, Constructor) {
		if (!(instance instanceof Constructor)) {
			throw new TypeError('Cannot call a class as a function');
		}
	}

	var _utilsGuid = __webpack_require__(2);

	var _utilsGuid2 = _interopRequireDefault(_utilsGuid);

	var _utilsDispatcher = __webpack_require__(3);

	var _utilsDispatcher2 = _interopRequireDefault(_utilsDispatcher);

	/**
  * @class Hi
  * @classdesc The main application for HI Projects.
  * @author Mark Scripter [mscripter@horizontalintegration.com]
  * @requires 'ramda'
  * @requires './utils/guid'
  */

	var Hi = (function () {

		/**
  * This is the constructor method for the Hi class.
  */

		function Hi() {
			_classCallCheck(this, Hi);

			// this.cache is for an internal cache of modules.
			// This is used to restore a module if needed.
			this.cache = {};
			this.channels = {};

			// this.registeredModules holds all modules that have been registered with the application.
			this.registeredModules = {};

			// this.dispatch is the main dispatch used throughout the application.
			// it allows us to send messages (subscribe/publish) and pass data along with it.
			this.dispatcher = _utilsDispatcher2['default'];
		}

		/**
  * This method takes a module, registers it if it doesn't exist and then wires up any DOM item that references the module.
  * @param {object} module - The module we want to register.
  * @returns
  */

		_createClass(Hi, [{
			key: 'registerModule',
			value: function registerModule(module) {
				/* Mathias Update -- Remove toLowerCase -- not sure what it does. @mathiasaurusrex */
			    var name = module.name;
				//var name = module.name.toLowerCase();
				if (!this.isRegistered(name)) {
					this.registeredModules[name] = module;

					// if our module is not cached,
					// Cache it,
					// otherwise continue.
					!this.isCached(name) ? this.cache[name] = module : 1;

					this.wireupModule(name);
				} else {
					// module is registered
					// wireup any new instances that were added to the DOM
					this.wireupModule(name);
				}
			}

			/**
   * This method takes a module, and unregisters it from the application.
   * @param {object} module - The module we want to unregister.
   * @returns {boolean} boolean - Returns true if successfully removed, otherwise false.
   */
		}, {
			key: 'unregisterModule',
			value: function unregisterModule(module) {
				var name = module.name.toLowerCase();
				if (this.isRegistered(name)) {
					delete this.registeredModules[name];
				}
			}

			/**
   * Takes an key and checks to see if it is registered within our registeredModules.
   * @param {string} key - The name of the module you want to verify.
   * @returns {boolean} boolean - Returns true if registered, otherwise false.
   */
		}, {
			key: 'isRegistered',
			value: function isRegistered(key) {
				return R.curry(function (registered, item) {
					return R.has(item, registered);
				})(this.registeredModules || {})(key);
			}

			/**
   * Takes an key and checks to see if it is cached  within our application.
   * @param {string} key - The name of the module you want to verify.
   * @returns {boolean} boolean - Returns true if registered, otherwise false.
   */
		}, {
			key: 'isCached',
			value: function isCached(key) {
				return R.curry(function (registered, item) {
					return R.has(item, registered);
				})(this.cache || {})(key);
			}

			/**
   * Takes a module that's referenced on the DOM, verifies the module is registered and instantiates it.
   * @param {object} module - The DOM element to instantiate a module from.
   * @returns
   */
		}, {
			key: 'instantiateModules',
			value: function instantiateModules(module) {
				var moduleName = module.getAttribute('data-module');
				if (this.isRegistered(moduleName)) {
					this.registeredModules[moduleName]({ target: module, id: (0, _utilsGuid2['default'])() });
				}
			}

			/**
   * A method that get's an array of modules, by name, from the DOM, iterates through the array and calls instantiateModules for each item.
   * @param {string} key - The name of the module you want to wireup.
   * @returns
   */
		}, {
			key: 'wireupModule',
			value: function wireupModule(key) {
				// run handlers and enhancers
				R.forEach(this.instantiateModules.bind(this), document.querySelectorAll('[data-module="' + key + '"]'));
			}

			/**
   *
   * @param {object} module -
   * @returns
   */
		}, {
			key: 'installTo',
			value: function installTo(obj) {
				obj.subscribe = this.subscribe;
				obj.publish = this.publish;
			}

			/**
   * :
   * @param {object} module -
   * @returns
   */
		}, {
			key: 'subscribe',
			value: function subscribe(channel, callback) {
				if (!this.channels[channel]) this.channels[channel] = [];
				this.channels[channel].push({ context: this, receive: callback });
				return this;
			}

			/**
   * :
   * @param {object} module -
   * @returns
   */
		}, {
			key: 'publish',
			value: function publish(channel, data) {
				if (!this.channels[channel]) return false;
				this.channels[channel].forEach(function (subscriber) {
					subscriber.receive(subscriber.context, data);
				});
			}
		}]);

		return Hi;
	})();

	exports['default'] = Hi;
	module.exports = exports['default'];

	/***/
},
/* 2 */
function (module, exports) {

	/**
  * This function creates a unique guid and returns it.
  * @module guid
  * @author Mark Scripter [mscripter@horizontalintegration.com]
  * @returns {guid} - A unique string similar to "aa1bbed7-f92e-da44-a5c5-54b23e170f2f"
  * @example
  * import guid from './guid';
  * var res = guid();
  * console.log(res);
  * // outputs a string similar to "aa1bbed7-f92e-da44-a5c5-54b23e170f2f"
  */
	'use strict';

	Object.defineProperty(exports, '__esModule', {
		value: true
	});
	var guid = function guid() {
		/**
  * Our 's4' method which returns a unique 4 character long string.
  * @function s4
  * @returns {string}
  * @private
  */
		var s4 = function s4() {
			return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
		};

		return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
	};

	exports['default'] = guid;
	module.exports = exports['default'];

	/***/
},
/* 3 */
function (module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, '__esModule', {
		value: true
	});

	function _interopRequireDefault(obj) {
		return obj && obj.__esModule ? obj : { 'default': obj };
	}

	var _stores = __webpack_require__(4);

	var _stores2 = _interopRequireDefault(_stores);

	/**
  * This object defines our dispatcher. It has a few methods & keys that extend onto a store implementation.
  * @module dispatcher
  * @author Mark Scripter [mscripter@horizontalintegration.com]
  * @requires stores
  * @returns {dispatch} - A dispatcher object
  */
	var dispatcher = {

		/**
  * This stores all of our data stores which can be accessible through the dispatcher.
  * @public
  * @example
  * this.dispatcher.stores
  */
		stores: _stores2['default'],

		/**
  * Our internal stores, used to iterate through and call each store.
  * @private
  */
		_stores: Object.keys(_stores2['default']).map(function (key) {
			return _stores2['default'][key];
		}),

		/**
  * Our 'on' method which allows us to subscribe|listen to an event.
  * @function on
  */
		on: function on() {
			var _arguments = arguments;

			this._stores.forEach(function (el) {
				el.on.apply(null, [].slice.call(_arguments));
			});
		},

		/**
  * Our 'one' method which allows us to listen to an event, once.
  * @function one
  */
		one: function one() {
			var _arguments2 = arguments;

			this._stores.forEach(function (el) {
				el.one.apply(null, [].slice.call(_arguments2));
			});
		},

		/**
  * Our 'off' method which allows us to unsubscribe|unlisten to an event.
  * @function off
  */
		off: function off() {
			var _arguments3 = arguments;

			this._stores.forEach(function (el) {
				el.off.apply(null, [].slice.call(_arguments3));
			});
		},

		/**
  * Our 'trigger' method which allows us to trigger an event.
  * @function trigger
  */
		trigger: function trigger() {
			var _arguments4 = arguments;

			this._stores.forEach(function (el) {
				el.trigger.apply(null, [].slice.call(_arguments4));
			});
		}

	};

	exports['default'] = dispatcher;
	module.exports = exports['default'];

	/***/
},
/* 4 */
function (module, exports) {

	// import recipe from './recipe';

	"use strict";

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports["default"] = {
		// recipe,
	};
	module.exports = exports["default"];

	/***/
},
/* 5 */
function (module, exports) {

	'use strict';

	Object.defineProperty(exports, '__esModule', {
		value: true
	});
	function Accordion(state) {
		var wireUp = function wireUp(settings) {
			$(state.target).colorbox(settings);
		};

		wireUp(JSON.parse(state.target.getAttribute('data-settings')));
	}

	exports['default'] = Accordion;
	module.exports = exports['default'];

	/***/
},
/* 6 */
function (module, exports) {

	// import R from 'ramda';

	'use strict';

	Object.defineProperty(exports, '__esModule', {
		value: true
	});
	function Modal(state) {
		var wireUp = function wireUp(settings) {
			$(state.target).colorbox(settings);
		};

		wireUp(R.merge(JSON.parse(state.target.getAttribute('data-settings')), { href: $(state.target).parent().find('.modal-content') }));
	}

	exports['default'] = Modal;
	module.exports = exports['default'];

	/***/
},
/* 7 */
function (module, exports) {

	'use strict';

	Object.defineProperty(exports, '__esModule', {
		value: true
	});
	function Carousel(state) {
		var wireUp = function wireUp(settings) {
			$(state.target).slick(settings);
		};

		wireUp(JSON.parse(state.target.getAttribute('data-settings')));
	}

	exports['default'] = Carousel;
	module.exports = exports['default'];

	/***/
},
/* 8 */
function (module, exports) {

	'use strict';

	$('.banner-carousel').slick({
		dots: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 2000
	});

	$('.hero-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.hero-sub-slider'
	});

	$('.hero-sub-slider').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		asNavFor: '.hero-slider',
		dots: true,
		centerMode: true,
		focusOnSelect: true
	});

	/***/
},
/* 9 */
function (module, exports) {

	// import cmd from './commands';

	'use strict';

	Object.defineProperty(exports, '__esModule', {
		value: true
	});
	function Form(state) {
		var wireUp = function wireUp(settings) {
			$(state.target).form(settings);
		};

		wireUp(JSON.parse(state.target.getAttribute('data-settings')));
	}

	exports['default'] = Form;
	module.exports = exports['default'];

	/***/
},
/* 10 */
function (module, exports) {

	'use strict';

	Object.defineProperty(exports, '__esModule', {
		value: true
	});
	function mobileMenuInit(state) {
		$('.mobile-menu').toggleClass('active');
		$('.mobile-menu-trigger--sub').removeClass('active');
	}

	function mobileMenuChildInit(state) {
		state.addClass('active');
	}

	function mobileMenuBack(state) {
		var closestActive = state.closest('ul').siblings();
		console.log(closestActive);
		closestActive.removeClass('active');
	}

	$('#mobile-menu-trigger').click(function (e) {
		e.preventDefault();
		mobileMenuInit();
	});

	$('.mobile-menu-trigger--sub').click(function (e) {
		var clickedElement = $(this);
		mobileMenuChildInit(clickedElement);
	});

	$('.mobile-menu--back').click(function (e) {
		var clickedElement = $(this);
		mobileMenuBack(clickedElement);
	});

	exports['default'] = mobileMenuInit;
	exports['default'] = mobileMenuChildInit;
	exports['default'] = mobileMenuBack;
	module.exports = exports['default'];

	/***/
},
/* 11 */
function (module, exports) {

	'use strict';

	Object.defineProperty(exports, '__esModule', {
		value: true
	});
	function defaultInteractiveAccordion(state) {
		var parentSpace = state.closest('.flex-space-interactive');
		parentSpace.toggleClass("active");
	}

	$('.flex-space-button').click(function (e) {
		var clickedElement = $(this);
		defaultInteractiveAccordion(clickedElement);
	});

	exports['default'] = defaultInteractiveAccordion;
	module.exports = exports['default'];

	/***/
}
/******/]);
/***/ /***/ /***/ /***/ /***/ /***/ /***/ /***/ /***/ /***/ /***/ /***/
/* 
 END /src/scripts/bundle.js 
 */ 
