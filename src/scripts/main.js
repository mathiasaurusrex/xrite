import Hi from './app/index.js';
import Accordion from '../components/accordion';
import Modal from '../components/modal';
import Carousel from '../components/carousel';
import Banner from '../components/banner';
import Form from '../components/form';
import Toggle from '../components/navigation';
import flexSpaceInteractive from '../components/flexspace-interactive';

const app = new Hi();

// register modules
app.registerModule(Accordion);
app.registerModule(Modal);
app.registerModule(Carousel);
app.registerModule(Form);