import babel from 'gulp-babel';
import browserSync from 'browser-sync';
import callbackSequence from 'callback-sequence';
import combiner from 'stream-combiner2';
import concat from 'gulp-concat';
import cssMinify from 'gulp-minify-css';
import documentation from 'gulp-documentation';
import eslint from 'gulp-eslint';
import glob from 'glob';
import gulp from 'gulp';
const  isparta = require('isparta');
import istanbul from 'gulp-istanbul';
import path from 'path';
import prefix from 'gulp-autoprefixer';
import R from 'ramda';
import rename from 'gulp-rename';
import sass from 'gulp-sass';
import sassdoc from 'sassdoc';
import sourcemaps from 'gulp-sourcemaps';
import svgstore from 'gulp-svgstore';
import svgmin from 'gulp-svgmin';
import tape from 'gulp-tape';
import templates  from 'gulp-jade';
import uglify from 'gulp-uglify';
import webpack from 'gulp-webpack';
import wrapper from 'gulp-wrapper';
import fs from 'fs';

const PATHS = require('./config.json').paths;

// js-test callbacks
const test = () => {
  return gulp.src(path.join(__dirname, PATHS.javascript, '/tests/index.js'))
    .pipe(tape());
};

const instrument = () => {
  return gulp.src([
    path.join(__dirname, PATHS.javascript, '/app/**/*.js'),
    path.join(__dirname, PATHS.javascript, '/app/*.js'),
  ])
    .pipe(istanbul({
      includeUntested: true,
      instrumenter: isparta.Instrumenter,
    }))
    .pipe(istanbul.hookRequire({}));
};

const report = () => {
  return gulp.src(path.join(__dirname, PATHS.javascript, '/tests/index.js'))
    .pipe(istanbul.writeReports({
      reporters: ['lcov', 'text'],
      reportOpts: { dir: path.join(__dirname, PATHS.public, 'coverage') },
    }));
};

gulp.task('default', ['build', 'serve']);

gulp.task('build', ['styles', 'javascript', 'assets', 'templates']);
gulp.task('serve', ['server']);

gulp.task('assets', ['svg', 'fonts', 'assetFolder']);
gulp.task('javascript', ['js-global', 'js-libraries', 'js-maps', 'js-docs', 'js-test']);
gulp.task('styles', ['sass', 'styleguide-styles', 'styles-documentation']);
gulp.task('templates', ['jade', 'styleguide-jade']);


gulp.task('assetFolder', () => {
  return gulp.src(path.join(__dirname, PATHS.assets, '**.*'))
    .pipe(gulp.dest(path.join(__dirname, PATHS.public, 'assets/')));
});

gulp.task('fonts', () => {
  return gulp.src(path.join(__dirname, PATHS.fonts))
    .pipe(gulp.dest(path.join(__dirname, PATHS.public, 'css/fonts/')));
});

gulp.task('jade', () => {
  // get glob of pages
  glob(path.join(__dirname, PATHS.templates), {}, (err, pages) => {
    if (err) return;

    // for each page
    pages.forEach((page) => {
      const data = require(path.join(__dirname, PATHS.data));
      gulp.src(page, {cwd: path.join(__dirname, 'src')})
        .pipe(templates({
          locals: data,
          pretty: true
        }))
        .pipe(gulp.dest(path.join(__dirname, PATHS.public)));
    });
  });

  return gulp.src(path.join(__dirname, PATHS.index))
    .pipe(templates({
      pretty: true
    }))
    .pipe(gulp.dest(path.join(__dirname, PATHS.public)));
});

gulp.task('js-docs', () => {
  glob(path.join(__dirname, PATHS.javascript, '/app/**/*.js'), {}, (err, pages) => {
    gulp.src([
      path.join(__dirname, PATHS.javascript, 'main.js'),
      ...pages,
    ])
      .pipe(documentation({format: 'html'}))
      .pipe(gulp.dest(path.join(__dirname, PATHS.public, 'jsdocs/')));
  });
  return;
});

gulp.task('js-global', () => {
  const combined = combiner.obj([
    gulp.src([
      path.join(__dirname, PATHS.javascript, 'main.js'),
    ]),
    webpack(require('./webpack.config.js')),
    eslint(),
    babel(),
    wrapper({
      header: '\n/* \n ${filename} \n */ \n',
      footer: '\n/* \n END ${filename} \n */ \n',
    }),
    sourcemaps.init(),
    concat('main.js'),
    gulp.dest(path.join(__dirname, PATHS.public, 'js/')),
    uglify({
      mangle: false,
      compress: true,
      // Compress handles minify -- Mathias
    }),
    rename('main.min.js'),
    gulp.dest(path.join(__dirname, PATHS.public, 'js/')),
  ]);

  return combined;
});

gulp.task('js-libraries', () => {
  const libs = PATHS.jsLibraries.map((filePath) => {
    return path.join(__dirname, filePath);
  });

  return combiner.obj([
    gulp.src(libs),
    eslint(),
    wrapper({
      header: '\n/* \n ${filename} \n */ \n',
      footer: '\n/* \n END ${filename} \n */ \n',
    }),
    concat('libs.min.js'),
    gulp.dest(path.join(__dirname, PATHS.public, 'js/')),
  ]);
});

gulp.task('js-maps', () => {
  const maps = PATHS.jsMaps.map((filePath) => {
    return path.join(__dirname, filePath);
  });

  return gulp.src(maps)
    .pipe(gulp.dest(path.join(__dirname, PATHS.public, 'js')));
});

gulp.task('js-test', callbackSequence(instrument, test, report));

gulp.task('sass', () => {
  const globalCSS = PATHS.cssLibraries.map((filePath) => {
    return path.join(__dirname, filePath);
  });
  return gulp.src([
    ...globalCSS,
    path.join(__dirname, PATHS.styles, 'main.scss'),
    path.join(__dirname, PATHS.components, '**/*.scss'),
  ])
    .pipe(sourcemaps.init())
    .pipe(concat('stylesheet.scss'))
    .pipe(sass({
      includePaths: [path.join(__dirname, PATHS.styles)],
    }))
    .pipe(prefix({
      browsers: ['last 4 versions'],
      cascade: 'false',
    }))
    .pipe(cssMinify())
    .pipe(rename('stylesheet.css'))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest(path.join(__dirname, PATHS.public, 'css/')));
});

gulp.task('server', ['watch'], () => {
  browserSync({
    server: {
      baseDir: 'public',
    },
  });
});

gulp.task('styles-documentation', () => {
  gulp.src([
    path.join(__dirname, PATHS.styles, 'global/**.scss'),
    path.join(__dirname, PATHS.components, '**/*.scss'),
    path.join(__dirname, PATHS.styles, 'global/**/*.scss'),
  ])
    .pipe(sassdoc());
});

gulp.task('styleguide-styles', () => {
  return gulp.src(path.join(__dirname, PATHS.styleguide.styles))
    .pipe(sass({
      includePaths: [path.join(__dirname, PATHS.styles)],
    }))
    .pipe(rename('styleguide.css'))
    .pipe(gulp.dest(path.join(__dirname, PATHS.public, 'css/')));
});

gulp.task('styleguide-jade', () => {
  const styleguideItems = PATHS.styleguide.templates.map((filePath) => {
    return path.join(__dirname, filePath);
  });

  //const data = require(path.join(__dirname, PATHS.styleguide.data));
  const data = fs.readFileSync(path.join(__dirname, PATHS.styleguide.data), 'utf-8', (err, data) => {
     return data;
   });
  // return gulp.src(styleguideItems)
  //   .pipe(templates({
  //     locals: data,
  //   }))
  //   .pipe(gulp.dest(path.join(__dirname, PATHS.public, 'styleguide/')));

  return gulp.src(styleguideItems)
     .pipe(templates({
       locals: JSON.parse(data),
       pretty: true
     }))
     .pipe(gulp.dest(path.join(__dirname, PATHS.public, 'styleguide/')));
});

gulp.task('svg', () => {
  return gulp.src(path.join(__dirname, PATHS.svg, '**.svg'))
    .pipe(svgmin())
    .pipe(svgstore())
    .pipe(gulp.dest(path.join(__dirname, PATHS.public, 'svg/')))
    .pipe(rename('svg.min.jade'))
    .pipe(gulp.dest(path.join(__dirname, PATHS.svg)));
});

gulp.task('watch', () => {
  gulp.watch([
    path.join(__dirname, PATHS.assets),
  ], ['assets'], () => {
    browserSync.reload;
  });

  gulp.watch([
    path.join(__dirname, PATHS.styles, '**.scss'),
    path.join(__dirname, PATHS.styles, '**/**.scss'),
    path.join(__dirname, PATHS.components, '**/**.scss'),
  ], ['styles'], () => browserSync.reload);

  gulp.watch([
    path.join(__dirname, PATHS.templates),
    path.join(__dirname, '/pages/**/*.jade'),
    path.join(__dirname, PATHS.components, '/**/markup/**.jade'),
    path.join(__dirname, PATHS.componentsData),
    path.join(__dirname, PATHS.data),
    PATHS.styleguide.templates.map((itemPath) => path.join(__dirname, itemPath)),
  ], ['templates'], () => browserSync.reload);

  const styleGuideTemplates = PATHS.styleguide.templates.map((template) => {
     return path.join(__dirname, template);
   });

  gulp.watch(
   [
     ...styleGuideTemplates,
     path.join(__dirname, PATHS.styleguide.styles),
     path.join(__dirname, PATHS.styleguide.data),
   ],
  ['styleguide-styles', 'styleguide-jade'], () => browserSync.reload);

  gulp.watch([
    path.join(__dirname, PATHS.javascript, '*.js'),
    path.join(__dirname, PATHS.javascript, '**/*.js'),
    path.join(__dirname, PATHS.javascript, '**/**/*.js'),
    path.join(__dirname, PATHS.components, '**/*.js'),
  ], ['javascript'], () => browserSync.reload);

  gulp.watch([
    path.join(__dirname, PATHS.svg, '*.svg'),
  ], ['templates'], () => browserSync.reload);
});
